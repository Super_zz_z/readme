---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Alice
nom: Briand

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---
# Profil

Bonjour, Je m'appelle Alice, j'ai 18 ans et je suis actuellement en première année de BTS SIO au lycée La Colinière à Nantes.

## Contact

Adresse mail : alice.briand4405@gmail.com  
Numéro de téléphone: 0670786887 

# Formation

* bac technologique en STI2D option SIN (systèmes d'information et numérique)
* BTS SIO (Services informatiques aux organisations)

# Compétence

| Compétence | Exemple |
| ---------- | ------- |
| Langues | Français, Anglais |
| Language de programmation | python, HTML, GO |
| Système d'exploitation | windows, linux |

# Experience

## Autre

* stage d'observation, en 3eme, à la FNAC de Nantes.
