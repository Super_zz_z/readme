---
prenom: Eddy
nom: Chasle
---
# **Profil**

![](https://cdn.discordapp.com/attachments/767361118741987369/1154422078943002673/Capture.PNG)

>Etudiant au sein de l'établissement au Lycée de la Colinière en BTS SIO. Passionné par l'informatique depuis 4 ans, je réalise des projets en tout genre. 

## Contact

 - **Numéro de téléphone** : [06 51 65 68 00](+330651656800)

 - **Adresse mail** : [eddy.chasle@gmail.com](eddy.chasle@gmail.com)

 - **Réseau social** : [LinkedIn](www.linkedin.com/in/eddy-chasle-1338a0265)

# Formation

- **Bac Général Spécialité Mathématiques & NSI** / _[Lycée Alcide D'Orbigny](https://alcide-orbigny.paysdelaloire.e-lyco.fr/) - Bouaye_ / 2019 - 2022
- **Expert en Technologies de l’Information** / _[Epitech](https://www.epitech.eu/fr/ecole-informatique-nantes/) - Nantes_ / 2022 - 2023
- **[BTS SIO - Service de l'Informatique aux Organisations](https://www.letudiant.fr/etudes/bts/bts-sio-services-informatiques-aux-organisations.html)** / _[Lycée de la Colinière](https://coliniere.paysdelaloire.e-lyco.fr/) - Nantes_ / 2023 - En cours / Avec pour continuation la spécialité [SLAM](https://www.onisep.fr/ressources/univers-formation/formations/Post-bac/bts-services-informatiques-aux-organisations-option-b-solutions-logicielles-et-applications-metiers)


# Compétences

| Compétences | Caractéristiques |
|- | :-|
| Langages de Programmation | Python, Javascript, C |
| Systèmes d'Exploitation | Windows, Ubuntu, Debian, Fedora |
| Langues | Français, Anglais |
| Divers | Autonomie, Gestion de projet, Travail d'équipe

# Expériences

- **Employé commercial** / _Hyper U - La Montagne_ / Juillet 2023 -  Août 2023
- **Agent d'entretien des espaces verts** / _La Montagne_ / Octobre 2021

## Expériences qui enrichissent mon choix pour la spécialité SLAM

- **Projet Epitodo**: Lors de mon année à Epitech, j'ai dû réaliser un projet en équipe de 3, qui consiste à créer une Todo List. C'est à dire que chaque utilisateur enregistré, peut émettre des listes de tâches sur leur compte. Ce projet avait pour but de nous initier aux langages SQL pour les traitements de données et JavaScript pour la gestion des requêtes
- **Projet IMC**: Avec ma spécialité NSI en première, j'ai du réaliser un site qui permet de calculer l'IMC d'une personne avec sa taille (en m) et son poids (en kg). Cela nous a permis de nous initier à l'HTML et au CSS. [Site IMC](https://site-imc-super-zz-z-ddc9c1008adb61a1c6cb818284f38c39ab32892f080.frama.io/IMC.html)
- **Projet 7 Familles**: Cette année, en BTS SIO, nous avons pour projet de créer un site web sur une des familles de la 7 familles sur le domaine l'informatique, donné au préalable, en équipe de 4 personnes. Toujours en cours.

## Stage

- **Stage d'observation 3eme** / _[Bouygues Energie & Service](https://www.bouygues-es.fr/) - Saint-Herblain_ / Décembre 2019

## Bénévolat

- **Evénementiel: Responsable de salle** / _[Web2Day](https://web2day.co/) - Nantes_ / Juin 2023
- **Employé service client** / _[DevFest](https://devfest2023.gdgnantes.com/) - Nantes_ / Octobre 2022
