---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Mohamed
nom: ZOUBLI

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# **Profile**

![alt text](data:image/)

Élève en BTS SIO(Services informatiques aux organisation) en spécialité SLAM(solutions logicielles et applications métiers) au lycée la Coloniére, j'ai acquis plusieurs compétences transversales au cours de mon parcours et de mes hobbies, tels que : la patience, le travail d'équipe et du leadership, un esprit critique et de mieux comprendre mon environnement.


## **Contact**

 -**numèro de téléphone** : 06-30-86-28-13

 -**mail** : [mohamedzoubli55@gmail.com](mailto:mohamedzoubli55@gmail.com)


- **Redirection vers des comptes professionnels**:
    - [Twitter](https://twitter.com/ZOUBLI_Mohamed)
    - [Linkedin](www.linkedin.com/in/mohamed-zoubli-223942293)


# **Compétence**

| Compétence       | Exemple          
|:-|:-:|
| Langue de programmation |   Python, Htlm      
| langues                 |   Français, Anglais           
| système d'exploitation  |   Windows, Linux


# **Éxpérience**

-



# **Éxpérience professionnelle**

- Je n'ai pas beaucoup d'expérience professionnelle à part les deux stages que j'ai pu effectuer, un en 3ème dans un garage automobile et un deuxième que j'ai réaliser durant les vacances d'été dans un magasin de réparation de téléphone.
